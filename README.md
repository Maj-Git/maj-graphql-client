# Maj GraphQL Client

## Install

Run `npm i maj-graphql-client`

## Usage

### With the following GraphQL schema :

```graphql endpoint
type Article {
    id: ID!
    title: String!
    content: String!
    comments: [Comment]!
}

type Comment {
    id: ID!
    text: String!
}

input ArticleInput {
    title: String!
    content: String!
}

input CommentInput {
    text: String!
}

type Query {
    articles: [Article]!
    articleById(id: ID): Article!
}

type Mutation {
    addArticle(article: ArticleInput!): Article
    addComment(articleId: ID!, comment: CommentInput!): Comment
}
```

### Building queries

```typescript
const fetchArticles: Query = new QueryBuilder('FetchArticles')
    .withPath('articles') // Query#articles from the schema.
    .withProperties(['id', 'title'])
    .buildQuery();

const fetchArticleById: Query = new QueryBuilder('FetchArticleById')
    .withPath('articleById') // Query#articleById from the schema.
    .withArgs([{name: 'id', type: 'ID!'}])
    .withProperties(['id', 'title', 'content', {'comments': ['id', 'text']}])
    .buildQuery();
```

```typescript
console.log(fetchArticles.toGQL());
```

Outputs :

```graphql
query FetchArticles {
    articles {
        id,
        title
    }
}
```

```typescript
console.log(fetchArticleById.toGQL());
```

Outputs :

```graphql
query FetchArticleById($id: ID!) {
    articleById(id: $id) {
        id,
        title,
        content,
        comments {
            id,
            text
        }
    }
}
```

### Building mutations

```typescript
const addArticle: Mutation = new QueryBuilder('AddArticle')
    .withPath('addArticle') // Mutation#addArticle from the schema.
    .withArgs([{name: 'article', type: 'ArticleInput!'}])
    .withProperties(['id', 'title'])
    .buildMutation();

const addComment: Mutation = new QueryBuilder('AddComment')
    .withPath('addComment') // Mutation#addComment from the schema.
    .withArgs([{name: 'articleId', type: 'ID!'}, {name: 'comment', type: 'CommentInput!'}])
    .withProperties(['id', 'text'])
    .buildMutation();
```

```typescript
console.log(addArticle.toGQL());
```

Outputs :

```graphql
mutation AddArticle($article: ArticleInput!) {
    addArticle(article: $article) {
        id,
        title
    }
}
```

```typescript
console.log(addComment.toGQL());
```

Outputs :

```graphql
mutation AddComment($articleId: ID!, $comment: CommentInput!) {
    addComment(articleId: $articleId, comment: $comment) {
        id,
        text
    }
}
```

### Minifying GQL queries
The `Query#toGQL()` method returns the query string in a readable format, with linebreaks, whitespaces and indentations, which is great for debugging.
But when sending the query through HTTP, those linebreaks, whitespaces and indentations are unnecessary and increase the payload size.

When sending the query string through HTTP, you can use the `Query#toMinifiedGQL()` method that will remove every unnecessary linebreak, whitespace and indentation but still return a valid GQL query.

### Sending a query through HTTP
When using your favorite HTTP client, maybe axios, fetch or Angular's own HttpClient, send a POST request to the GraphQL server 
with a body such as :

````typescript
{
    query: string;
    variables: [key: string]: any
}
````

Example
````typescript
const fetchArticleById: Query = new QueryBuilder('FetchArticleById')
    .withPath('articleById') // Query#articleById from the schema.
    .withArgs([{name: 'id', type: 'ID!'}])
    .withProperties(['id', 'title', 'content', {'comments': ['id', 'text']}])
    .buildQuery();

const id = 1;

http.post('graphql-server-url', {
    query : fetchArticleById.toMinifiedGQL(),
    variables: {
        id: 1
    }
})
````

### Additional notes
`Query` and `Mutation` use lazy attributes to store the resulting GQL query strings, `Query#toGQL()` and `Query#toMinifiedGQL()` will only compute the GQL query string the first time they are called, after that, stored query strings will be returned directly. 