import {Query, QueryKind} from "./query";

export class Mutation extends Query {
    type: QueryKind = 'mutation';
}