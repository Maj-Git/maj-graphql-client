import {QueryBuilder} from "../query-builder";

/**
 * @see QueryBuilder
 * @see Query
 * @see GqlStringBuilder
 * @see GqlStringMinifier
 */
describe('Tests for the Query class. Also covers classes GqlStringBuilder and GqlStringMinifier.', () => {
    test('Building a query with an empty name should throw an error', () => {
        const query = new QueryBuilder('')
            .buildQuery();

        expect(() => query.toGQL()).toThrowError('A query name is required.');
    });

    test('Building a query with an empty path should throw an error', () => {
        const query = new QueryBuilder('QueryName')
            .buildQuery();

        expect(() => query.toGQL()).toThrowError('A query path is required.');
    });

    test('Building a query without properties should throw an error', () => {
        const query = new QueryBuilder('QueryName')
            .withPath("QueryPath")
            .buildQuery();

        expect(() => query.toGQL()).toThrowError('Query properties are required.');
    });

    test('Building a query with missing nested properties should throw an error', () => {
        const query = new QueryBuilder('QueryName')
            .withPath("QueryPath")
            .withProperties(['id', 'title', {'nested': []}])
            .buildQuery();

        expect(() => query.toGQL()).toThrowError('Query properties are required.');
    });

    test('Building a query should return a rightfully constructed query', () => {
        const query = new QueryBuilder('QueryName')
            .withPath("queryPath")
            .withProperties(['id', 'title', {'nested': ['id', 'description']}])
            .buildQuery();

        const expected =
            `query QueryName {
  queryPath {
    id,
    title,
    nested {
      id,
      description
    }
  }
}`;

        const expectedMinified = `query QueryName{queryPath{id,title,nested{id,description}}}`;

        expect(query.toGQL()).toEqual(expected);
        expect(query.toMinifiedGQL()).toEqual(expectedMinified);
    });

    test('Building a query with even more nested properties should return a rightfully constructed query', () => {
        const query = new QueryBuilder('QueryName')
            .withPath("queryPath")
            .withProperties(['id', 'title', {'nested': ['id', 'description', {'nested2': ['id', 'description']}]}, {'nested3': ['id', 'description']}])
            .buildQuery();

        const expected =
            `query QueryName {
  queryPath {
    id,
    title,
    nested {
      id,
      description,
      nested2 {
        id,
        description
      }
    },
    nested3 {
      id,
      description
    }
  }
}`;

        const expectedMinified = `query QueryName{queryPath{id,title,nested{id,description,nested2{id,description}},nested3{id,description}}}`;

        expect(query.toGQL()).toEqual(expected);
        expect(query.toMinifiedGQL()).toEqual(expectedMinified);
    });

    test('Building a query with arguments should return a rightfully constructed query', () => {
        const query = new QueryBuilder('QueryName')
            .withPath("queryPath")
            .withArgs([{name: 'id', type: 'ID!'}])
            .withProperties(['id', 'title', {'nested': ['id', 'description']}])
            .buildQuery();

        const expected =
            `query QueryName($id: ID!) {
  queryPath(id: $id) {
    id,
    title,
    nested {
      id,
      description
    }
  }
}`;

        const expectedMinified = `query QueryName($id:ID!){queryPath(id:$id){id,title,nested{id,description}}}`;

        expect(query.toGQL()).toEqual(expected);
        expect(query.toMinifiedGQL()).toEqual(expectedMinified);
    });
});

