import {QueryBuilder} from "../query-builder";

/**
 * @see QueryBuilder
 * @see Mutation
 * @see GqlStringBuilder
 * @see GqlStringMinifier
 */
describe('Tests for the Mutation class. Also covers classes GqlStringBuilder and GqlStringMinifier.', () => {
    test('Building a mutation with an empty name should throw an error', () => {
        const mutation = new QueryBuilder('')
            .buildMutation();

        expect(() => mutation.toGQL()).toThrowError('A mutation name is required.');
    });

    test('Building a mutation with an empty path should throw an error', () => {
        const mutation = new QueryBuilder('MutationName')
            .buildMutation();

        expect(() => mutation.toGQL()).toThrowError('A mutation path is required.');
    });

    test('Building a mutation without properties should throw an error', () => {
        const mutation = new QueryBuilder('MutationName')
            .withPath("MutationPath")
            .buildMutation();

        expect(() => mutation.toGQL()).toThrowError('Mutation properties are required.');
    });

    test('Building a mutation with missing nested properties should throw an error', () => {
        const mutation = new QueryBuilder('MutationName')
            .withPath("MutationPath")
            .withProperties(['id', 'title', {'nested': []}])
            .buildMutation();

       expect(() => mutation.toGQL()).toThrowError('Mutation properties are required.');
    });

    test('Building a mutation should return a rightfully constructed mutation', () => {
        const mutation = new QueryBuilder('MutationName')
            .withPath("mutationPath")
            .withProperties(['id', 'title', {'nested': ['id', 'description']}])
            .buildMutation();

        const expected =
            `mutation MutationName {
  mutationPath {
    id,
    title,
    nested {
      id,
      description
    }
  }
}`;

        const expectedMinified = `mutation MutationName{mutationPath{id,title,nested{id,description}}}`;

        expect(mutation.toGQL()).toEqual(expected);
        expect(mutation.toMinifiedGQL()).toEqual(expectedMinified);
    });

    test('Building a mutation with even more nested properties should return a rightfully constructed mutation', () => {
        const mutation = new QueryBuilder('MutationName')
            .withPath("mutationPath")
            .withProperties(['id', 'title', {'nested': ['id', 'description', {'nested2': ['id', 'description']}]}, {'nested3': ['id', 'description']}])
            .buildMutation();

        const expected =
            `mutation MutationName {
  mutationPath {
    id,
    title,
    nested {
      id,
      description,
      nested2 {
        id,
        description
      }
    },
    nested3 {
      id,
      description
    }
  }
}`;

        const expectedMinified = `mutation MutationName{mutationPath{id,title,nested{id,description,nested2{id,description}},nested3{id,description}}}`;

        expect(mutation.toGQL()).toEqual(expected);
        expect(mutation.toMinifiedGQL()).toEqual(expectedMinified);
    });

    test('Building a mutation with arguments should return a rightfully constructed mutation', () => {
        const mutation = new QueryBuilder('MutationName')
            .withPath("mutationPath")
            .withArgs([{name: 'id', type: 'ID!'}])
            .withProperties(['id', 'title', {'nested': ['id', 'description']}])
            .buildMutation();

        const expected =
            `mutation MutationName($id: ID!) {
  mutationPath(id: $id) {
    id,
    title,
    nested {
      id,
      description
    }
  }
}`;

        const expectedMinified = `mutation MutationName($id:ID!){mutationPath(id:$id){id,title,nested{id,description}}}`;

        expect(mutation.toGQL()).toEqual(expected);
        expect(mutation.toMinifiedGQL()).toEqual(expectedMinified);
    });
});

