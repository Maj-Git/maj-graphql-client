import {QueryBuilder} from "../query-builder";
import {Query} from "../query";
import {Mutation} from "../mutation";

const name = 'Name';

/**
 * @see QueryBuilder
 * @see Query
 */
describe('Tests for the QueryBuilder class', () => {
    test('QueryBuilder#constructor test return type', () => {
        const builder = new QueryBuilder(name);
        const query = builder.buildQuery();
        const mutation = builder
            .buildMutation();

        expect(query instanceof Query).toBeTruthy();
        expect(mutation instanceof Mutation).toBeTruthy();
    });

    test('QueryBuilder#constructor', () => {
        const builder = new QueryBuilder(name);
        const query = builder.buildQuery();
        const mutation = builder
            .buildMutation();

        expect(query.name).toEqual(name);
        expect(mutation.name).toEqual(name);
    });

    test('QueryBuilder#withPath', () => {
        const path = 'path';
        const builder = new QueryBuilder(name)
            .withPath(path);
        const query = builder.buildQuery();
        const mutation = builder
            .buildMutation();

        expect(query.path).toEqual(path);
        expect(mutation.path).toEqual(path);
    });

    test('Query Builder#withProperties', () => {
        const properties = ['id', 'title', {'nested': ['id', 'description']}];
        let builder = new QueryBuilder(name)
            .withProperties(properties);
        const query = builder.buildQuery();
        const mutation = builder
            .buildMutation()

        expect(query.properties).toEqual(properties);
        expect(mutation.properties).toEqual(properties);
    });

    test('Query Builder#withArgs', () => {
        const args = [{name: 'id', type: 'ID!'}, {name: 'arg2', type: 'String'}];
        let builder = new QueryBuilder(name)
            .withArgs(args);
        const query = builder.buildQuery();
        const mutation = builder
            .buildMutation();

        expect(query.args).toEqual(args);
        expect(mutation.args).toEqual(args);
    });
});