import {IQuery, Argument, Property} from "./query";

/**
 * The GqlStringBuilder class.
 *
 * Allows the user to build the GraphQL query string from a query object.
 */
export class GqlStringBuilder {

    /**
     * The result GQL query string.
     * @private
     */
    private gql: string = '';

    /**
     * Private constructor.
     * Use the static of() method instead.
     *
     * @param query The query for which to build a GQL string.
     * @private
     */
    private constructor(private readonly query: IQuery) {
    }

    /**
     * Static named constructor.
     *
     * @param query  The query for which to build a GQL string.
     *
     * @returns A new GqlStringBuilder object.
     */
    static of(query: IQuery): GqlStringBuilder {
        return new GqlStringBuilder(query);
    }

    /**
     * Builds the GQL query string and returns it.
     */
    public build(): string {
        if (!this.query.name || this.query.name.length == 0)
            throw new Error(`A ${this.query.type} name is required.`);

        if (!this.query.path || this.query.path.length == 0)
            throw new Error(`A ${this.query.type} path is required.`);

        if (!this.query.properties || this.query.properties.length == 0) {
            const queryTypeCapitalized = this.query.type.charAt(0).toUpperCase() + this.query.type.slice(1);
            throw new Error(`${queryTypeCapitalized} properties are required.`);
        }

        this.gql = '';

        const shouldHandleArgs = this.query.args && this.query.args.length > 0;

        this.appendKeyword();
        this.appendSpace();
        this.appendQueryName();
        if (shouldHandleArgs) {
            this.appendArgsDefinition();
        }
        this.appendSpace();
        this.appendLeftCurlyBracket();

        this.appendNewLine();
        this.appendIndents(1);

        this.appendQueryPath();
        if (shouldHandleArgs) {
            this.appendArgs();
        }
        this.appendSpace();
        this.appendLeftCurlyBracket();

        this.appendNewLine();
        this.appendProperties(this.query.properties, 1);

        this.appendNewLine();
        this.appendIndents(1);
        this.appendRightCurlyBracket();

        this.appendNewLine();
        this.appendRightCurlyBracket();

        return this.gql;
    }

    /**
     * Appends the appropriate keyword to the start of the GQL query string "query" or "mutation".
     * @private
     */
    private appendKeyword(): void {
        this.gql += this.query.type;
    }

    /**
     * Appends the query name to the GQL query string.
     * @private
     */
    private appendQueryName(): void {
        this.gql += this.query.name;
    }

    /**
     * Appends the query path to the GQL query string.
     * @private
     */
    private appendQueryPath(): void {
        this.gql += this.query.path;
    }

    /**
     * Appends the query arguments definition to the GQL query string after the query name.
     *
     * "query QueryName($argName: argType, ...)"
     * @private
     */
    private appendArgsDefinition(): void {
        const argString: (arg: Argument) => string = arg => `$${arg.name}: ${arg.type}`;
        this.gql += `(${this.query.args.map(argString).join(', ')})`;
    }

    /**
     * Appends the query arguments values to the GQL query string after the query path.
     *
     * "queryPath(argName: $argName)"
     * @private
     */
    private appendArgs(): void {
        const argString: (arg: Argument) => string = arg => `${arg.name}: $${arg.name}`;
        this.gql += `(${this.query.args.map(argString).join(', ')})`;
    }

    /**
     * Appends the query properties to the GQL query string after the query path (and arguments if present).
     *
     * "queryPath(argName: $argName) {
     *   stringProperty,
     *   objectProperty {
     *       nestedProperty,
     *       ...
     *   },
     *   ...
     * }"
     * @private
     */
    private appendProperties(props: Property[], level: number): void {
        props.forEach((prop, index) => {
            this.appendIndents(level + 1);
            if (typeof prop === 'string') {
                this.gql += `${prop}`;
            } else {
                for (const key in prop) {
                    if (!prop[key] || prop[key].length == 0) {
                        const queryTypeCapitalized = this.query.type.charAt(0).toUpperCase() + this.query.type.slice(1);
                        throw new Error(`${queryTypeCapitalized} properties are required.`);
                    }

                    this.gql += key;
                    this.appendSpace();
                    this.appendLeftCurlyBracket();

                    this.appendNewLine();

                    this.appendProperties(prop[key], level + 1);

                    this.appendNewLine();

                    this.appendIndents(level + 1);
                    this.appendRightCurlyBracket();
                }
            }

            if (index < props.length - 1) {
                this.gql += ',';
                this.appendNewLine();
            }
        });
    }

    /**
     * Appends a space to the GQL query string.
     * @private
     */
    private appendSpace(): void {
        this.gql += ' ';
    }

    /**
     * Appends a left curly bracket to the GQL query string.
     * @private
     */
    private appendLeftCurlyBracket(): void {
        this.gql += '{';
    }

    /**
     * Appends a right curly bracket to the GQL query string.
     * @private
     */
    private appendRightCurlyBracket(): void {
        this.gql += '}';
    }

    /**
     * Appends a new line curly bracket to the GQL query string.
     * @private
     */
    private appendNewLine() {
        this.gql += `
`;
    }

    /**
     * Appends indents to the GQL query string.
     *
     * @param number The number of indents to append.
     * @private
     */
    private appendIndents(number: number): void {
        for (let i = 0; i < number; i++) {
            this.gql += `  `;
        }
    }
}