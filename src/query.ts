import {Lazy} from "./utils/lazy";
import {GqlStringBuilder} from "./gql-string-builder";
import {GqlStringMinifier} from "./gql-string-minifier";

/**
 * Query or Mutation discrimination value.
 */
export type QueryKind = 'query' | 'mutation'


/**
 * Argument type.
 */
export type Argument = { name: string, type: string };

/**
 * Property type.
 *
 * A property can either be a string or a nested object with properties.
 */
export type Property = string | { [key: string]: Property[] };

/**
 * Internal typing interface.
 */
export interface IQuery {
    name: string;
    path: string;
    properties: Property[];
    args: Argument[];
    type: QueryKind;
}

/**
 * The Query class.
 */
export class Query implements IQuery {
    /**
     * The kind of query.
     */
    type: QueryKind = 'query';

    /**
     * The query path.
     *
     * This is the name of the query defined in the GraphQL schema.
     */
    path!: string;

    /**
     * The query properties.
     */
    properties: Property [] = [];

    /**
     * The query arguments.
     */
    args: Argument[] = []

    /**
     * The lazy representation of this Query object in its GQL string form.
     *
     * The GQL is in a human, readable format, containing spaces, line breaks and indents.
     * When sending the GQL through HTTP, using the minified GQL will help reduce payload size.
     *
     * @private
     */
    private gql: Lazy<string> = Lazy.of(() => GqlStringBuilder.of(this).build());

    /**
     * The lazy representation of this Query object in its GQL minified string form.
     *
     * The GQL is minified, stripped of all unnecessary spaces, line breaks and indent for a reduced payload size.
     *
     * @private
     */
    private minifiedGql: Lazy<string> = Lazy.of(() => GqlStringMinifier.of(this).minify());

    /**
     * Public constructor.
     * @param name The query name.
     */
    constructor(readonly name: string) {
    }

    /**
     * Returns the human, readable format of the GQL.
     *
     * @returns The GQL.
     */
    public toGQL(): string {
       return this.gql.get();
    }

    /**
     * Returns the minified format of the GQL.
     *
     * @returns The minified GQL.
     */
    public toMinifiedGQL(): string {
        return this.minifiedGql.get();
    }
}
