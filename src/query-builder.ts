import {Argument, Property, Query} from "./query";
import {Mutation} from "./mutation";

/**
 * Builds a {@link Query} object step by step.
 */
export class QueryBuilder {
    /**
     * The resulting {@link Query}.
     * @private
     */
    private readonly query: Query

    /**
     * Public constructor.
     * @param queryName The query name.
     */
    constructor(readonly queryName: string) {
        this.query = new Query(queryName);
    }

    /**
     * Sets the path of the wrapped query object and returns this builder.
     *
     * @param path The query path.
     * @returns This builder.
     */
    public withPath(path: string): QueryBuilder {
        this.query.path = path;

        return this;
    }

    /**
     * Sets the properties of the wrapped query object and returns this builder.
     *
     * @param properties The query properties.
     * @returns This builder.
     */
    public withProperties(properties: Property[]): QueryBuilder {
        this.query.properties = properties;
        return this;
    }

    /**
     * Sets the args of the wrapped query object and returns this builder.
     *
     * @param args The query args.
     * @returns This builder.
     */
    public withArgs(args: Argument[]): QueryBuilder {
        this.query.args = args;
        return this;
    }

    /**
     * Returns the wrapped query.
     * @returns The {@link Query} object.
     */
    public buildQuery(): Query {
        return this.query;
    }

    /**
     * Returns a new mutation from the wrapped query.
     * @returns The {@link Mutation} object.
     */
    public buildMutation(): Mutation {
        const {name, path, properties, args} = this.query;
        const mutation = new Mutation(name);
        mutation.path = path;
        mutation.properties = properties;
        mutation.args = args;

        return mutation
    }
}