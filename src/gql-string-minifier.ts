import {Query} from "./query";

/**
 * The GqlStringMinifier class.
 *
 * Allows the user to retrieve a minified GraphQL query string from a {@link Query} object.
 */
export class GqlStringMinifier {

    /**
     * Private constructor.
     * Use the static of() method instead.
     *
     * @param query The query for which to build a GQL string.
     * @private
     */
    private constructor(private readonly query: Query) {
    }

    /**
     * Static named constructor.
     *
     * @param query  The query for which to build a GQL string.
     *
     * @returns A new GqlStringBuilder object.
     */
    static of(query: Query): GqlStringMinifier {
        return new GqlStringMinifier(query);
    }

    /**
     * Gets the GQL query string from the {@link Query} and returns it minified without
     * the unnecessary whitespaces, line breaks and indents.
     */
    public minify(): string {
        const gql = this.query.toGQL();
        const firstSpace = gql.indexOf(` `);

        const left = gql.substring(0, firstSpace + 1);
        const right = gql.substring(firstSpace).replace(/(\r\n|\r|\n| )/g, '');

        return left.concat(right);
    }
}