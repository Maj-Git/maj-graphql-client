type LazyInitializer<T> = () => T;

/**
 * Lazy wrapper for a value.
 *
 * The value will remain null until the get() method is called.
 * The initializer will then be called to populate the value.
 *
 * After the initializer has been called, populated value will always be returned.
 */
export class Lazy<T> {
    private value: T | null = null;

    /**
     * Private constructor.
     * Use the static of() method instead.
     *
     * @param initializer The value initializer.
     * @private
     */
    private constructor(private readonly initializer: LazyInitializer<T>) {
    }

    /**
     * Static named constructor.
     *
     * @param initializer The value initializer.
     *
     * @returns A new Lazy object.
     */
    static of<T>(initializer: LazyInitializer<T>): Lazy<T> {
        return new Lazy<T>(initializer);
    }

    /**
     * Initializes the wrapped value if initializer hasn't been called yet, then
     * returns the value.
     *
     * @returns The wrapped value.
     */
    get(): T {
        if (this.value == null) {
            this.value = this.initializer();
        }

        return this.value;
    }
}